﻿using UnityEngine;

public class PacMan : MonoBehaviour 
{
    public int MoveSpeed = 5;
    public GameObject[] path;
    private Animator animator;
    private int x;
    private int y;
    private float des;
    int index = 0;


    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    enum Dir
    {
        Up,
        Down,
        Left,
        Right
    }

    private void Update()
    {
        /*if (Input.GetKey(KeyCode.W))
        {
            Move(Dir.Up);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Move(Dir.Down);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Move(Dir.Left);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Move(Dir.Right);
        }*/
        if (index==0)
        {
            x = -1;
            y = 0;
        }
        else if (index == 1)
        {
            x = 0;
            y = 1;
        }
        else if (index == 2)
        {
            x = 1;
            y = 0;
        }
        else if (index == 3)
        {
            x = 0;
            y = -1;
        }
        animator.SetFloat("X", x);
        animator.SetFloat("Y", y);
        des = Vector3.Distance(this.transform.position, path[index].transform.position);
        transform.position = Vector3.MoveTowards(this.transform.position, path[index].transform.position, Time.deltaTime * MoveSpeed);

        if (des < 0.1f )
        {
            index++;
            if (index >= path.Length)
            {
                index = 0;
            }
        }
     
        

    }

    private void Move(Dir dir)
    {
        switch (dir)
        {
            case Dir.Up:
                x = 0;
                y = 1;
                break;
            case Dir.Down:
                x = 0;
                y = -1;
                break;
            case Dir.Left:
                x = -1;
                y = 0;
                break;
            case Dir.Right:
                x = 1;
                y = 0;
                break;
            default:
                break;
        }
        animator.SetFloat("X", x);
        animator.SetFloat("Y", y);


        transform.position += new Vector3(x * MoveSpeed*Time.deltaTime, y * MoveSpeed * Time.deltaTime, 0);

    }
}