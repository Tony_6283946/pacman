﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour 
{
    public GameObject outsideWall;
    public GameObject insideWall;
    public GameObject pellet;
    public GameObject powerPellets;
    private int[,] levelMapUp;
    private int[,] levelMapDown;

    private void Start()
    {
        levelMapUp = new int[,]    {
                                  {1,2,2,2,2,2,2,2,2,2,2,2,2,7,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                  {2,5,5,5,5,5,5,5,5,5,5,5,5,4,5,5,5,5,5,5,5,5,5,5,5,5,2},
                                  {2,5,3,4,4,3,5,3,4,4,4,3,5,4,5,3,4,4,4,3,5,3,4,4,3,5,2},
                                  {2,6,4,0,0,4,5,4,0,0,0,4,5,4,5,4,0,0,0,4,5,4,0,0,4,6,2},
                                  {2,5,3,4,4,3,5,3,4,4,4,3,5,3,5,3,4,4,4,3,5,3,4,4,3,5,2},
                                  {2,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,2},
                                  {2,5,3,4,4,3,5,3,3,5,3,4,4,4,4,4,3,5,3,3,5,3,4,4,3,5,2},
                                  {2,5,3,4,4,3,5,4,4,5,3,4,4,3,4,4,3,5,4,4,5,3,4,4,3,5,2},
                                  {2,5,5,5,5,5,5,4,4,5,5,5,5,4,5,5,5,5,4,4,5,5,5,5,5,5,2},
                                  {1,2,2,2,2,1,5,4,3,4,4,3,0,4,0,3,4,4,3,4,5,1,2,2,2,2,1},
                                  {0,0,0,0,0,2,5,4,3,4,4,3,0,3,0,3,4,4,3,4,5,2,0,0,0,0,0},
                                  {0,0,0,0,0,2,5,4,4,0,0,0,0,0,0,0,0,0,4,4,5,2,0,0,0,0,0},
                                  {0,0,0,0,0,2,5,4,4,0,3,4,4,0,4,4,3,0,4,4,5,2,0,0,0,0,0},
                                  {2,2,2,2,2,1,5,3,3,0,4,0,0,0,0,0,4,0,3,3,5,1,2,2,2,2,2},
                                  {0,0,0,0,0,0,5,0,0,0,4,0,0,0,0,0,4,0,0,0,5,0,0,0,0,0,0},
                                  };

        levelMapDown = new int[,]    {
                                  {1,2,2,2,2,2,2,2,2,2,2,2,2,7,2,2,2,2,2,2,2,2,2,2,2,2,1},
                                  {2,5,5,5,5,5,5,5,5,5,5,5,5,4,5,5,5,5,5,5,5,5,5,5,5,5,2},
                                  {2,5,3,4,4,3,5,3,4,4,4,3,5,4,5,3,4,4,4,3,5,3,4,4,3,5,2},
                                  {2,6,4,0,0,4,5,4,0,0,0,4,5,4,5,4,0,0,0,4,5,4,0,0,4,6,2},
                                  {2,5,3,4,4,3,5,3,4,4,4,3,5,3,5,3,4,4,4,3,5,3,4,4,3,5,2},
                                  {2,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,2},
                                  {2,5,3,4,4,3,5,3,3,5,3,4,4,4,4,4,3,5,3,3,5,3,4,4,3,5,2},
                                  {2,5,3,4,4,3,5,4,4,5,3,4,4,3,4,4,3,5,4,4,5,3,4,4,3,5,2},
                                  {2,5,5,5,5,5,5,4,4,5,5,5,5,4,5,5,5,5,4,4,5,5,5,5,5,5,2},
                                  {1,2,2,2,2,1,5,4,3,4,4,3,0,4,0,3,4,4,3,4,5,1,2,2,2,2,1},
                                  {0,0,0,0,0,2,5,4,3,4,4,3,0,3,0,3,4,4,3,4,5,2,0,0,0,0,0},
                                  {0,0,0,0,0,2,5,4,4,0,0,0,0,0,0,0,0,0,4,4,5,2,0,0,0,0,0},
                                  {0,0,0,0,0,2,5,4,4,0,3,4,4,0,4,4,3,0,4,4,5,2,0,0,0,0,0},
                                  {2,2,2,2,2,1,5,3,3,0,4,0,0,0,0,0,4,0,3,3,5,1,2,2,2,2,2},
                                  };
        InitMap();

    }


    private void InitMap()
    {
        for (int i = 0; i < levelMapUp.GetLength(0); i++)
        {
            for (int j = 0; j < levelMapUp.GetLength(1); j++)
            {
                GameObject go = null;
                if (levelMapUp[i, j] == 1|| levelMapUp[i, j] == 2|| levelMapUp[i, j] == 7)
                {
                    go = Instantiate(outsideWall);
                }
                if (levelMapUp[i, j] == 3 || levelMapUp[i, j] == 4)
                {
                    go = Instantiate(insideWall);
                }
                if (levelMapUp[i, j] == 5)
                {
                    go = Instantiate(pellet);
                }
                if (levelMapUp[i, j] == 6)
                {
                    go = Instantiate(powerPellets);
                }
                if (go!=null)
                {
                    go.transform.position = new Vector3(j, levelMapUp.GetLength(0) - i - 1, 0);
                    go.transform.SetParent(this.transform);
                }
                
            }
        }

        for (int i = 0; i < levelMapDown.GetLength(0); i++)
        {
            for (int j = 0; j < levelMapDown.GetLength(1); j++)
            {
                GameObject go = null;
                if (levelMapDown[i, j] == 1 || levelMapDown[i, j] == 2 || levelMapDown[i, j] == 7)
                {
                    go = Instantiate(outsideWall);
                }
                if (levelMapDown[i, j] == 3 || levelMapDown[i, j] == 4)
                {
                    go = Instantiate(insideWall);
                }
                if (levelMapDown[i, j] == 5)
                {
                    go = Instantiate(pellet);
                }
                if (levelMapDown[i, j] == 6)
                {
                    go = Instantiate(powerPellets);
                }
                if (go != null)
                {
                    go.transform.position = new Vector3(j, -levelMapDown.GetLength(0) + i , 0);
                    go.transform.SetParent(this.transform);
                }

            }
        }
    }

}