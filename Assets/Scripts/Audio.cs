﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Audio : MonoBehaviour 
{
    public AudioClip gameIntro;
    public AudioClip normal;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = gameIntro;
        audioSource.loop = false;
        audioSource.Play();
        StartCoroutine(AudioPlayFinished(audioSource.clip.length));
    }

    private IEnumerator AudioPlayFinished(float time)
    {
        yield return new WaitForSeconds(time);

        audioSource.clip = normal;
        audioSource.loop = true;
        audioSource.Play();

    }

}